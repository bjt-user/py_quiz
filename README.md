You need an xml file that contains the questions and the answers formatted like this:
```
<?xml version="1.0"?>
<quizfile>
  <quizblock>
    <question>How can you ensure that all (or some) nodes run a copy of a pod?</question>
    <answer>By using a DaemonSet.</answer>
  </quizblock>
  <quizblock>
    <question>Is this a test?</question>
    <answer>yes</answer>
  </quizblock>
  <quizblock>
    <question>foo?</question>
    <answer>bar</answer>
  </quizblock>
</quizfile>
```

#### TODO

- put answer evaluation in function
- do not count case mistakes (treat everything as lower or upper case inside the script)
--> easy to implement, but you dont want that if you ask for commands (which are case sensitive)
--> maybe add an optional "accuracy" tag to the xml for quizblocks

- ignore periods at the end of sentences for answers (not for commands though)
