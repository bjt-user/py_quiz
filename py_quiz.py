#!/usr/bin/python3

import sys
import argparse
import random
import xml.etree.ElementTree as ET

def questionFinder():
    pos1 = 0

    for z in range(0, selected_question_id):
        try:
            pos1 = text.index("<question>", pos1)
            pos1 += 1
        except:
            print("index out of range")
            break
    # lastly remove the leading question tag
    pos1 += 9
    return pos1

def userInput(answer_list, user_input_list):
    userAction = ""
    for l in range(0, len(answer_list)):
        newInput = input()
        user_input_list.append(newInput)
        if newInput == "quit":
            userAction = "quit"
            break
            
    return userAction

parser = argparse.ArgumentParser()
parser.add_argument('-q', '--quizfile', help='Path to the quiz file')
args = parser.parse_args()

if len(sys.argv) <= 1:
    parser.print_help()
    exit(1)


quiz_file = args.quizfile

tree = ET.parse(quiz_file)
root = tree.getroot()

user_input_list = []

answeredQuestions = []

points = 0

mistakes = 0

# booleans seem to be case sensitive and None for defining a bool with no value

# path for the open-function is entered in unix style even on windows systems

try:
    daten = open(quiz_file, "r")
    text = daten.read()    
except:
    print("cannot open file")

daten.close()


number_of_questions = len(root)
print("number of questions:" + str(number_of_questions))

alreadyAnswered = "firstLoop"

# Python has no static typing but you have to define variables (Python weirdness)
# and to define a variable you have to initialize it for some reason
i = 0

while True:
    # it seems to be impossible to only answer a question once
    # booleans may be broken in Python, so I try it with Strings
    # one of Pythons weaknesses is that they change a lot of the syntax of existing languages
    # like writing True and False with startin capital letters

    selected_question_id = random.randint(0, number_of_questions-1)

    if alreadyAnswered != "firstLoop":
        for j in range(0, len(answeredQuestions)):
            if selected_question_id == answeredQuestions[j]:
                alreadyAnswered = "yes"

    if alreadyAnswered != "yes":
        answeredQuestions.append(selected_question_id)
    else:
        alreadyAnswered = "blank"
        continue

    # for debugging purposes only (the questions were numbered before shuffling)
    print(str(answeredQuestions))

    print("The randomly selected question is question number " + str(selected_question_id))
    sqid = questionFinder()

    question_list = root[selected_question_id].findall('question')

    answer_list = root[selected_question_id].findall('answer')


    # the question number refer now to the real order of events
    print("Question " + str(i) + ": ", end='')
    for question in question_list:
        print (question.text)

    # for debugging purposes:
    #for answer in answer_list:
    #    print (answer.text)

    # user input
    userAction = userInput(answer_list, user_input_list)
    if userAction == "quit":
        print("you have quit the program")
        break

    for n in range(0, len(answer_list)):
        if str(user_input_list[n]).lower() == str(answer_list[n].text).lower():
            print("you were correct in line " + str(n))
            points += 1
        elif str(user_input_list[n]).lower().__contains__(str(answer_list[n].text).lower()):
            print("your solution contains the answer in line " + str(n))
            points += 0.5
        else:
            print("---the answer in line " + str(n) + " was:---\n<" + answer_list[n].text + ">")
            print("---but you said it was:---\n<" + str(user_input_list[n]) + ">")
            mistakes += 1

    print("You have " + str(points) + " points. You made " + str(mistakes) + " mistakes.")
    print()

    user_input_list.clear()
    i +=1
    if i == number_of_questions:
        break

    alreadyAnswered = "blank"



# i have to use the random.shuffle method next time
# but i had to proove that i could do it this way

# issue1: Antworten, die sich über mehrere Zeilen erstrecken
# eine Loesung koennte sein, die user_input_list und die answer_list jeweils in einen String umzuwandeln und dann zu vergleichen ob sie übereinstimmen
# so waere es dann egal wann man in einer Antwort den Zeilenumbruch macht
# wobei das wiederum bei mehrteiligen Antworten, die klar voneinander abzugrenzen sind, nicht so gut waere

# oder ich mache einfach in der quizdatei einen Zeilenumbruch und replace einfach "\n" mit "" und schließe den answer-tag erst nach mehreren Zeilen,
# wenn die Antwort wirklich zuende ist
# nur wie mache ich das dann beim Userinput?
